//
//  ContactObject.swift
//  ScreenovateTest
//
//  Created by Refael Sommer on 30/05/2021.
//

import Foundation

struct UsersResponse: Decodable {
    var users: [User]
}

struct User: Decodable {
    var id: Int
    var name, profileImage, username, email: String
    var address: Address
}

struct Address: Decodable {
    var street, suite, city, zipcode: String
    var geo: Geo
}

public struct Geo: Decodable {
    var lat, lng: String
}
