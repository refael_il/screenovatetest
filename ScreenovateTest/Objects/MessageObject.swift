//
//  MessageObject.swift
//  ScreenovateTest
//
//  Created by Refael Sommer on 30/05/2021.
//

import Foundation

struct MessagesResponse: Decodable, Encodable {
    var messages: [MessageObject]
}

struct MessageObject: Decodable, Encodable {
    var id: Int
    var senderId: Int
    var text: String
}
