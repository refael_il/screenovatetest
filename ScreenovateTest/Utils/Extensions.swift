//
//  Extensions.swift
//  ScreenovateTest
//
//  Created by Refael Sommer on 30/05/2021.
//

import Foundation

extension Dictionary {
    var jsonData: Data? {
        guard let theJSONData = try? JSONSerialization.data(withJSONObject: self,
                                                            options: [.prettyPrinted]) else {
            return nil
        }

        return theJSONData
    }
    
    func jsonString() -> String? {
        if let data = self.jsonData {
            if let jsonString = String(data: data, encoding: .utf8) {
                return jsonString
            }
        }
        return nil
    }
}

extension Data {
    func jsonString() -> String? {
        if let jsonString = String(data: self, encoding: .utf8) {
                return jsonString
        }
        return nil
    }
}

extension String {
    func convertToDictionary() -> [String: Any]? {
        if let data = self.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
}
