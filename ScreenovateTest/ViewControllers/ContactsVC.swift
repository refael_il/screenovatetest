//
//  ViewController.swift
//  ScreenovateTest
//
//  Created by Refael Sommer on 30/05/2021.
//

import UIKit
import SDWebImage

class ContactsVC: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {
    @IBOutlet weak var cvContacts: UICollectionView!
    var users: [User] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        loadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let collectionViewLayout = cvContacts.collectionViewLayout as? UICollectionViewFlowLayout {
            collectionViewLayout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            collectionViewLayout.scrollDirection = .vertical
            collectionViewLayout.estimatedItemSize = CGSize(
                width: cvContacts.frame.size.width,
                // Make the height a reasonable estimate to
                // ensure the scroll bar remains smooth
                height: 80
            )
        }
    }
    
    func loadData() {
        guard let users = ServerHandler.getUsers()?.users else { return }
        self.users.append(contentsOf: users)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return users.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ContactCell", for: indexPath) as! ContactCell
        let user = users[indexPath.item]
        let profileURL = URL(string: user.profileImage)
        cell.ivUser.sd_setImage(with: profileURL, completed: nil)
        cell.ivUser.layer.cornerRadius = cell.ivUser.frame.size.height/2
        cell.ivUser.contentMode = .scaleAspectFill
        cell.lblName.text = user.name
        cell.lblLastMessage.text = "last message will show here..."
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("didSelectItemAt")
        if let chatVC = self.storyboard?.instantiateViewController(withIdentifier: "ChatVC") as? ChatVC {
            let contact = users[indexPath.item]
            chatVC.recipientId = contact.id
            self.present(chatVC, animated: true, completion: nil)
        }
    }
}

