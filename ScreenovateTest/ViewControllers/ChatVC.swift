//
//  ChatVC.swift
//  ScreenovateTest
//
//  Created by Refael Sommer on 30/05/2021.
//

import Foundation

import UIKit

class ChatVC: UIViewController {
    
    @IBOutlet weak var viewToolbarBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var viewToolbarBottom: UIView!
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var tableView: UITableView!
    
    var recipientId: Int?
    var messages: [MessageObject] = []
    
    var cellIdentifier = "SenderCell"
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.estimatedRowHeight = 50
        tableView.rowHeight = UITableView.automaticDimension
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardHandle), name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardHandle), name: UIResponder.keyboardWillShowNotification, object: nil)

    }
    
    @objc func keyboardHandle(notification:NSNotification){
        if let userInfo = notification.userInfo{
            let keyboardFrameHeight = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue.size.height
            let isKeyboardShowing = (notification.name == UIResponder.keyboardWillShowNotification)
            if isKeyboardShowing {
                self.viewToolbarBottomConstraint.constant = keyboardFrameHeight - 30
            }
            else{
                self.viewToolbarBottomConstraint.constant = 0
            }
            self.view.layoutIfNeeded()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        loadData()
        //textField.becomeFirstResponder()
    }
    
    func loadData() {
        guard let id = recipientId else { return }
        guard let messageResponse = ServerHandler.getMessages(recipientId: id) else { return }
        messages.append(contentsOf: messageResponse.messages)
        tableView.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    @IBAction func sendText(_ sender: Any) {
        guard let text = textField.text, let recipientId = self.recipientId else { return }
        let messageObject = MessageObject(id: Int(RAND_MAX), senderId: recipientId, text: text)
        messages.append(messageObject)
        cellIdentifier = CellIdetifier.sender.rawValue
        let index = IndexPath(row: messages.count - 1, section: 0)
        tableView.insertRows(at: [index], with: .automatic)
        textField.text = ""
        
        ServerHandler.saveMessages(messagesResponse: MessagesResponse(messages: messages), recipientId: recipientId)
        
        self.perform(#selector(createAutoMessageBack(text:)), with: text, afterDelay: 1)
    }
    
    @objc func createAutoMessageBack(text: String) {
        let autoMessageBack = MessageObject(id: Int(RAND_MAX), senderId: 0, text: text)
        didRecieve(message: autoMessageBack)
    }
    
    func didRecieve(message: MessageObject) {
        DispatchQueue.main.async {
            self.cellIdentifier = CellIdetifier.reciever.rawValue
            self.messages.append(message)
            let index = IndexPath(row: self.messages.count - 1, section: 0)
            self.tableView.insertRows(at: [index], with: .automatic)
            
            ServerHandler.saveMessages(messagesResponse: MessagesResponse(messages: self.messages), recipientId: self.recipientId)
        }
    }
}

extension ChatVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let messageObject = messages[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: (messageObject.senderId == recipientId ? CellIdetifier.sender : CellIdetifier.reciever).rawValue) as! ChatTableViewCell
        cell.lblText.text = messageObject.text
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messages.count
    }
}

enum CellIdetifier: String {
    case sender = "SenderCell",
         reciever = "RecieverCell"
}
