//
//  ServerHandler.swift
//  ScreenovateTest
//
//  Created by Refael Sommer on 30/05/2021.
//

import Foundation

class ServerHandler: NSObject {
    static func getUsers() -> UsersResponse? {
        guard let usersJsonData = Utils.readLocalFile(forName: "Users") else { return nil }
        do {
            let usersResponse = try JSONDecoder().decode(UsersResponse.self, from: usersJsonData)
            return usersResponse
        } catch let error {
            print("decode error \(error)")
        }
        return nil
    }
    
    static func getMessages(recipientId: Int) -> MessagesResponse? {
        guard let messagesJsonData = UserDefaults.standard.object(forKey: "messages_\(recipientId)") as? Data else { return nil }
        do {
            let messagesResponse = try JSONDecoder().decode(MessagesResponse.self, from: messagesJsonData)
            return messagesResponse
        } catch let error {
            print("decode error \(error)")
        }
        return nil
    }
    
    static func saveMessages(messagesResponse: MessagesResponse, recipientId: Int?) {
        guard let recipientId = recipientId  else { return }
        let encoder = JSONEncoder()
        encoder.outputFormatting = .prettyPrinted

        do {
            let jsonData = try encoder.encode(messagesResponse)
            UserDefaults.standard.setValue(jsonData, forKey: "messages_\(recipientId)")
            
            if let jsonString = String(data: jsonData, encoding: .utf8) {
                print(jsonString)
            }
        } catch {
            print(error.localizedDescription)
        }
    }
}
