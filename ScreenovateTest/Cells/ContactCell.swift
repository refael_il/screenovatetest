//
//  ContactCell.swift
//  ScreenovateTest
//
//  Created by Refael Sommer on 30/05/2021.
//

import UIKit
import Foundation

class ContactCell: UICollectionViewCell {
    @IBOutlet weak var ivUser: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblLastMessage: UILabel!
}
