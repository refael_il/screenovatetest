//
//  ChatTableViewCell.swift
//  ScreenovateTest
//
//  Created by Refael Sommer on 30/05/2021.
//

import UIKit

class ChatTableViewCell: UITableViewCell {

    @IBOutlet weak var lblText: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
